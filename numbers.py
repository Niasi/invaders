from PPlay.sprite import *
import math

digits = {
    0: "images/0.png",
    1: "images/1.png",
    2: "images/2.png",
    3: "images/3.png",
    4: "images/4.png",
    5: "images/5.png",
    6: "images/6.png",
    7: "images/7.png",
    8: "images/8.png",
    9: "images/9.png"
}

bossDigits = {
    0: "images/boss/0.png",
    1: "images/boss/1.png",
    2: "images/boss/2.png",
    3: "images/boss/3.png",
    4: "images/boss/4.png",
    5: "images/boss/5.png",
    6: "images/boss/6.png",
    7: "images/boss/7.png",
    8: "images/boss/8.png",
    9: "images/boss/9.png",
    10: "images/boss/percent.png"
}

class Digit:

    def __init__(self, number, pos_x, pos_y):
        self.texture = Sprite(digits[number])
        self.texture.set_position(pos_x, pos_y)
        self.x = self.texture.x
        self.y = self.texture.y
        self.width = self.texture.width
        self.height = self.texture.height
        self.number = number

    def setPosition(self, lastPos):
        self.texture.x = lastPos.texture.x + lastPos.texture.width

    def changePosition(self, pos):
        self.texture.x = pos

    def draw(self):
        self.texture.draw()

class BossDigit:

    def __init__(self, number, pos_x, pos_y):
        self.texture = Sprite(bossDigits[number])
        self.texture.set_position(pos_x, pos_y)
        self.x = self.texture.x
        self.y = self.texture.y
        self.width = self.texture.width
        self.height = self.texture.height

    def draw(self):
        self.texture.draw()

def generate(number, startX, startY):
    digitMatrix = []
    while (number > 9):
        digit = math.floor((10.0*(number/10.0 - math.floor(number/10.0))))
        number = math.floor(number/10.0)
        digitMatrix.append(Digit(digit, startX, startY))
    digitMatrix.append(Digit(number, startX, startY))
    lsize = math.ceil(len(digitMatrix)/2.0) - len(digitMatrix)%2
    for i in range(0,lsize,1):
        temp = digitMatrix[i]
        digitMatrix[i] = digitMatrix[len(digitMatrix)-(1+i)]
        digitMatrix[len(digitMatrix) - (1+i)] = temp

    for i in range(1,len(digitMatrix),1):
        digitMatrix[i].setPosition(digitMatrix[i-1])
    return digitMatrix

def generateBossHealth(number, startX, startY):
    digitMatrix = []
    spacing = 14
    if number <= 0:
        return digitMatrix
    elif number >= 100:
        digitMatrix.append(BossDigit(1, startX, startY))
        digitMatrix.append(BossDigit(0, startX + spacing, startY))
        digitMatrix.append(BossDigit(0, startX + 2*spacing, startY))
        digitMatrix.append(BossDigit(10, startX + 3*spacing, startY))
    else:
        if (number > 9):
            digitMatrix.append(BossDigit(math.floor(number/10), startX, startY))
            digitMatrix.append(BossDigit(number%10, startX + spacing, startY))
            digitMatrix.append(BossDigit(10, startX + 2*spacing, startY))
        else:
            digitMatrix.append(BossDigit(number, startX, startY))
            digitMatrix.append(BossDigit(10, startX + spacing, startY))
    return digitMatrix