from PPlay.window import *
from PPlay.gameimage import *

import button


class Menu:
    def menu(d):
        window = Window(400, 600)
        window.set_title("Space Invaders")
        background = GameImage("images/fundo.png")
        backgroundDarken = GameImage("images/darken.png")
        difficulty = GameImage("images/btns/" + dif_string(d) + "-selected.png")

        mouse = window.get_mouse()

        buttonPlay = button.Button(window, "Play", 80)
        buttonDifficulty = button.Button(window, "Mode", 80+120)
        buttonRanking = button.Button(window, "Ranking", 80+120*2)
        buttonQuit = button.Button(window, "Quit", 80+120*3)

        difficulty.x = buttonDifficulty.x + buttonDifficulty.width - 20 - difficulty.width/2
        difficulty.y = buttonDifficulty.y + buttonDifficulty.height - 20 - difficulty.height/2

        while True:
            background.draw()
            backgroundDarken.draw()

            buttonPlay.draw(mouse)
            buttonDifficulty.draw(mouse)
            buttonRanking.draw(mouse)
            buttonQuit.draw(mouse)
            difficulty.draw()

            if buttonPlay.click(mouse):
                startGame(d)
                return None

            if buttonDifficulty.click(mouse):
                selectDifficulty(d)
                return None

            if buttonRanking.click(mouse):
                listRanking(d)
                return None

            if buttonQuit.click(mouse):
                if mouse.is_button_pressed(1):
                    window.close()

            #janela.draw_text("Dificuldade: " + s_d, 10, janela.height - 30, size=20, color=(255, 255, 255))
            window.update()

def startGame(d):
    from game import jogo
    jogo(d)

def dif_string(n):
    if n == 1:
        return "Easy"
    elif n == 2:
        return "Medium"
    elif n == 3:
        return "Hard"

def selectDifficulty(d):
    from difficultySelection import Difficulty
    Difficulty(d)


def listRanking(d):
    from ranking import Ranking
    Ranking(d)
