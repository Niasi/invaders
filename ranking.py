from PPlay.window import *
from PPlay.gameimage import *

def Ranking(difficulty):
    window = Window(400, 600)
    window.set_title("Space Invaders - Ranking")
    background = GameImage("images/fundo.png")
    backgroundDarken = GameImage("images/darken.png")
    title = GameImage("images/ranking.png")

    title.x = window.width/2 - title.width/2
    title.y = 60

    keyboard = window.get_keyboard()

    rank = open("score/score.txt", "r")
    l = rank.readlines()
    for i in range(len(l)):
        l[i] = l[i][0:len(l[i]) - 1]

    while True:
        background.draw()
        backgroundDarken.draw()
        title.draw()

        y = 60 + title.height + 80
        for i in l:
            i = i.split("$")
            window.draw_text(i[0], 10, y, size=35, color=(255, 255, 255), bold=True)
            window.draw_text(i[1], 240, y, size=40, color=(255, 255, 255), bold=True)
            y += 50

        if keyboard.key_pressed("esc"):
            rank.close()
            returnMenu(difficulty)

        window.update()


def returnMenu(difficulty):
    from menu import Menu
    Menu.menu(difficulty)


def sort():
    rank = open("score/score.txt", "r")

    l = rank.readlines()
    for i in range(len(l)):
        l[i] = l[i].split('$')

    l.sort(key=lambda a: int(a[1]), reverse=True)

    for i in range(len(l)):
        l[i] = l[i][0] + "$" + l[i][1]

    rank.close()
    rank = open("score/score.txt", "w")
    if len(l) >= 5:
        for i in range(5):
            rank.write(l[i])
    else:
        for i in range(len(l)):
            rank.write(l[i])
    rank.close()
