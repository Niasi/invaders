from PPlay.sprite import *
from random import randint
import shoot
import math

switcherSize = 3
switcher = {
        0: "images/alien.png",
        1: "images/jelly.png",
        2: "images/squid.png"
    }

class Alien:

    def __init__(self, pos_x, pos_y, textureId):
        self.texture = Sprite(switcher[textureId], 2)
        self.texture.set_total_duration(1500)
        self.texture.set_position(pos_x, pos_y)
        self.x = self.texture.x
        self.y = self.texture.y
        self.width = self.texture.width
        self.height = self.texture.height
        self.health = 1
        self.identity = 2

    def moveH(self, window, gap):
        self.texture.move_x(gap)
        if self.texture.x + gap <= window.width - 32 and self.texture.x + gap >= 0:
            return 1
        else:
            return -1

    def moveV(self, window, player):
        self.texture.move_y(4)
        if self.texture.height +  self.texture.y >= window.height - player.texture.height:
            player.health = 0

    def draw(self):
        self.texture.update()
        self.texture.draw()

class Boss:

    def __init__(self, pos_x, pos_y, difficulty):
        self.texture = Sprite("images/boss.png", 3)
        self.texture.set_total_duration(500)
        self.x = self.texture.x
        self.y = self.texture.y
        self.width = self.texture.width
        self.height = self.texture.height
        self.texture.set_position(pos_x-self.width/2, pos_y)
        self.health = difficulty*5
        self.identity = 3

    def moveH(self, window, gap):
        self.texture.move_x(gap)
        if self.texture.x + gap <= window.width - self.width and self.texture.x + gap >= 0:
            return 1
        else:
            return -1

    def moveV(self, window, player):
        self.texture.move_y(2)
        if self.texture.height +  self.texture.y >= window.height - player.texture.height:
            player.health = 0

    def draw(self):
        self.texture.update()
        self.texture.draw()

def generate(sizeA, sizeB):
    aliens = []
    for i in range(16, sizeB*32+1, 32):
        aliens_line = []
        for j in range(16, sizeA*48-15, 48):
            aliens_line.append(Alien(j,i,len(aliens)%switcherSize))
        aliens.append(aliens_line)
    return aliens

def generateBoss(window, difficulty):
    aliens = []
    aliens_line = []
    aliens_line.append(Boss(window.width/2,10, difficulty))
    aliens.append(aliens_line)
    return aliens

def getBossPercent(aliens,difficulty):
    if len(aliens) > 0:
        boss = aliens[0][0]
        return math.ceil(20*boss.health/difficulty)
    return 0

def allienShoot(aliens, shoots, window):
    if len(aliens) >0:
        line = randint(0,len(aliens)-1)
        row =  randint(0,len(aliens[line])-1)
        shoots.append(shoot.Shoot(window,aliens[line][row],400))

def drawEnemy(aliens):
    for matrix in aliens:
        for alien in matrix:
            alien.draw()