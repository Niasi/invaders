from PPlay.window import *
from PPlay.gameimage import *
from PPlay.sprite import *
from tkinter import *
import enemy
import user
import numbers

def dif_string(n):
    if n == 1:
        return "Easy"
    elif n == 2:
        return "Medium"
    elif n == 3:
        return "Hard"

def jogo(difficulty):
    window = Window(400, 600)
    window.set_title("Space Invaders")
    background = GameImage("images/fundo.png")
    keyboard = window.get_keyboard()

    gameover = GameImage("images/Game-Over.png")
    score = GameImage("images/Score.png")
    playAgain = GameImage("images/Press-R.png")

    gameover.x = window.width/2 - gameover.width/2
    gameover.y = 80
    score.x = window.width / 2 - score.width / 2
    score.y = window.height/2 - score.height/2
    playAgain.x = window.width / 2 - playAgain.width / 2
    playAgain.y = window.height - 80

    levelA = [3, 3, 4, 5, 6, 7, 8, 8, 8]
    levelB = [1, 2, 2, 3, 3, 4, 4, 5, 6]
    userName = ""
    bossLevel = 2
    level = 1
    enemies = enemy.generate(levelA[level-1],levelB[level-1])
    player = user.User(window)

    oneHeart = GameImage("images/one-heart.png")
    twoHearts = GameImage("images/two-hearts.png")
    threeHearts = GameImage("images/three-hearts.png")
    oneHeart.x = window.width - oneHeart.width
    twoHearts.x = window.width - twoHearts.width
    threeHearts.x = window.width - threeHearts.width

    congrats = Sprite("images/congrats.png", 3)
    congrats.set_total_duration(500)
    congrats.x = window.width/2 - congrats.width/2
    congrats.y = 80

    turn = 1
    shoots =[]
    moveLastAction = 0
    moveCooldown = 1500/difficulty
    shootLastAction = 0
    shootCooldown = 2400/difficulty
    stageTime = 0
    moveGap = 16
    fps = segundo = fps2 = 0

    while True:
        background.draw()

        if player.health > 0 and not player.winner:
            # Move o player
            player.move(keyboard, window)

            # Cria tiro do Player
            player.shoot(window, shoots)

            # Move os Aliens
            if turn != 1: #vertical
                if window.time_elapsed() - moveLastAction >= moveCooldown/abs(8):
                    moveLastAction = window.time_elapsed()
                    for i in enemies:
                        for j in i:
                            j.moveV(window, player)
                    turn += 2/abs(8)
            else: #horizontal
                if window.time_elapsed() - moveLastAction >= moveCooldown:
                    moveLastAction = window.time_elapsed()
                    for i in enemies:
                        for j in i:
                            turn = min(j.moveH(window, moveGap),turn)
                    moveGap *= turn

            # Tiros dos aliens
            if window.time_elapsed() - shootLastAction >= shootCooldown:
                shootLastAction = window.time_elapsed()
                enemy.allienShoot(enemies,shoots,window)

            # Tiros player
            for i in shoots:
                #move o projetil
                i.move(window)
                # Verifica o ceiling do ambiente
                i.shootCeiling(shoots)
                # verifica o acerto do 20projetil
                i.landShoot(player, enemies, shoots, level, difficulty, 3.14159*(40000/(window.time_elapsed()-stageTime)))

            # Desenha o player e os aliens
            player.draw(window)
            enemy.drawEnemy(enemies)

            # Número de vidas
            if player.health == 3:
                threeHearts.draw()
            elif player.health == 2:
                twoHearts.draw()
            elif player.health == 1:
                oneHeart.draw()

            # boss health
            if level == bossLevel:
                healthPercent = enemy.getBossPercent(enemies, difficulty)
                bossHealth = numbers.generateBossHealth(healthPercent, window.width/2 - 15, 15)
                for i in bossHealth:
                    i.draw()

            #Player Score
            digitScore = numbers.generate(player.score, 5, window.height - 25)
            for i in digitScore:
                i.draw()

            #Nivel concluido
            if len(enemies) == 0:
                shoots.clear()
                level += 1
                stageTime = window.time_elapsed()
                if level < bossLevel:
                    moveGap = 16
                    enemies = enemy.generate(levelA[level - 1], levelB[level - 1])
                elif level == bossLevel:
                    moveGap = 8
                    enemies= enemy.generateBoss(window, difficulty)
                    moveCooldown = int(moveCooldown*0.2)
                    shootCooldown = int(shootCooldown * 0.5)
                else:
                    player.winner = True

        # Game Over
        else:
            if (userName == ""):
                master = Tk()
                master.title("Player name")
                e = Entry(master)
                e.pack()

                def callback(*args):
                    global userWrite
                    userWrite = e.get()
                    master.destroy()
                e.focus_set()
                master.bind('<Return>', callback)

                b = Button(master, text="OK", width=10, command=callback)
                b.pack()

                mainloop()
                userName = userWrite[0:11]
                if userName == "":
                    userName = "blank"
                player.game_over(userName)

            if player.winner:
                congrats.update()
                congrats.draw()

            else:
                gameover.draw()

            score.draw()
            playAgain.draw()
            digitScore = numbers.generate(player.score, window.width / 2 - 60, window.height / 2 - 10)

            for i in range(0,len(digitScore),1):
                if i==0:
                    digitScore[i].changePosition(window.width / 2 - len(digitScore) * 7)
                else:
                    digitScore[i].setPosition(digitScore[i-1])
                digitScore[i].draw()


            if keyboard.key_pressed("R"):
                level = 1
                enemies = enemy.generate(levelA[level - 1], levelB[level - 1])
                player = user.User(window)
                shoots = []
                moveLastAction = 0
                shootLastAction = 0
                moveCooldown = 1500 / difficulty
                shootCooldown = 2400 / difficulty

        # Volta ao menu ao apertar "Esc"
        if keyboard.key_pressed("esc"):
            from menu import Menu
            Menu.menu(difficulty)

        segundo += window.delta_time()
        fps += 1
        if segundo >= 1:
            fps2 = fps
            fps = 0
            segundo = 0
        digits = numbers.generate(fps2, 5, 5)
        for i in digits:
            i.draw()

        window.update()