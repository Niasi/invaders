from PPlay.sprite import *
from random import randint
import math

class Shoot:

    def __init__(self, window, creature, speed):
        self.window = window
        self.creature = creature
        self.texture = Sprite("images/tiro.png")
        self.width = self.texture.width
        self.height = self.texture.height
        if creature.identity < 3:
            self.texture.set_position(creature.texture.x + creature.width/2 - self.width/2, creature.texture.y)
        else: #boos shoot
            self.texture.set_position(creature.texture.x + randint(0,1) * creature.width/2 + randint(0,2) * creature.width/6, creature.texture.y + creature.height)
        self.speed = speed

    def move(self, window):
        self.texture.move_y(self.speed * window.delta_time())
        self.texture.draw()

    def erase(self, shoots):
        if self in shoots:
            shoots.remove(self)

    def shootCeiling(self, shoots):
        if self.texture.y + self.height <= 0 or self.texture.y + self.height >= self.window.height:
            self.erase(shoots)

    def landShoot(self, player, aliens, shoots, level, difficulty, elapsedBonus):
        if self.creature != player:
            if self.texture.collided_perfect(player.texture):
                self.erase(shoots)
                player.damaged()
        else:
            for line in aliens:
                for alien in line:
                    if self.texture.collided(alien.texture):
                        self.erase(shoots)
                        alien.health -= 1
                        if alien.health == 0:
                            line.remove(alien)
                        player.score += math.ceil(elapsedBonus*level*(difficulty*difficulty))
                if len(line) == 0:
                    aliens.remove(line)

    def draw(self):
        self.texture.draw()