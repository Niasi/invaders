from PPlay.sprite import *
import shoot
import ranking

class User:

    def __init__(self, window):
        self.window = window
        self.keyboard = window.get_keyboard()
        self.texture = Sprite("images/ship.png")

        self.width = self.texture.width
        self.height = self.texture.height
        self.texture.set_position((self.window.width / 2) - (self.width / 2), self.window.height - (self.height))
        self.x = self.texture.x
        self.y = self.texture.y

        self.shootLastAction = 0
        self.shootCooldown = 500
        self.speed = 300

        self.health = 3
        self.winner = False
        self.score = 0

        self.blinking = 0
        self.lastBlink = 0
        self.intervalBlink = 200
        self.identity = 1

    def move(self, keyboard, window):
        if keyboard.key_pressed("right") and self.texture.x < self.window.width - self.width/2:
            self.texture.move_x(self.speed * window.delta_time())
        if keyboard.key_pressed("left") and self.texture.x > 0 - self.width/2:
            self.texture.move_x(- self.speed * window.delta_time())
        self.x = self.texture.x

    def shoot(self, window, shoots):
        if (self.keyboard.key_pressed("space") or self.keyboard.key_pressed("up")) and (window.time_elapsed() - self.shootLastAction) >= self.shootCooldown:
            shoots.append(shoot.Shoot(window, self, -400))
            self.shootLastAction = window.time_elapsed()

    def damaged(self):
        if self.blinking == 0:
            self.blinking = 10
            self.health -= 1

    def decreaseScore(self):
        if self.score > 0:
            self.score -= 1

    def game_over(self, user):
        rank = open("score/score.txt", "a")
        rank.write(user + "$" + str(self.score) + "\n")
        rank.close()
        ranking.sort()

    def draw(self, window):
        if window.time_elapsed() - self.lastBlink >= self.intervalBlink:
            self.lastBlink = window.time_elapsed()
            self.blinking = max(self.blinking - 1, 0)
        if (self.blinking % 2 == 0):
            self.texture.draw()