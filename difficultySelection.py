from PPlay.window import *
from PPlay.gameimage import *

import button


def Difficulty(d):
    window = Window(400, 600)
    window.set_title("Space Invaders - Mode Selector")
    background = GameImage("images/fundo.png")
    backgroundDarken = GameImage("images/darken.png")

    buttonEasy = button.Button(window, "Easy", 80)
    buttonMedium = button.Button(window, "Medium", 80+120)
    buttonHard = button.Button(window, "Hard", 80+120*2)

    keyboard = window.get_keyboard()
    mouse = window.get_mouse()

    while True:
        background.draw()
        backgroundDarken.draw()

        buttonEasy.draw(mouse)
        buttonMedium.draw(mouse)
        buttonHard.draw(mouse)

        if buttonEasy.click(mouse):
            returning(1)
        elif buttonMedium.click(mouse):
            returning(2)
        elif buttonHard.click(mouse):
            returning(3)

        if keyboard.key_pressed("esc"):
            returning(d)

        window.update()


def returning(d):
    from menu import Menu
    Menu.menu(d)