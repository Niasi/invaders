from PPlay.gameimage import *

class Button:

    def __init__(self, window, path, pos):
        self.window = window
        self.texture = GameImage("images/btns/" + path + ".png")
        self.textureGlow = GameImage("images/btns/" + path + "-glow.png")
        self.textureGlow.set_position(self.window.width / 2 - self.textureGlow.width / 2, pos)
        self.texture.set_position(self.window.width / 2 - self.texture.width / 2, pos)

        self.x = self.texture.x
        self.y = self.texture.y

        self.width = self.texture.width
        self.height = self.texture.height

    def click(self, mouse):
        if mouse.is_over_area((self.x, self.y), (self.x + self.width, self.y + self.height)):
            if mouse.is_button_pressed(1):
                return True

    def draw(self, mouse):
        if mouse.is_over_area((self.x, self.y), (self.x + self.width, self.y + self.height)):
            self.textureGlow.draw()
        else:
            self.texture.draw()

